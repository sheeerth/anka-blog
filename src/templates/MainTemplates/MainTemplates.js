import React from "react"
import PropTypes from "prop-types"
import Header from '../../components/Header/header'
import Sidebar from './../../components/Sidebar/Sidebar';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./main-layout.css"
import Blur from "../../components/Blur/Blur";
import styled from 'styled-components';
import {Footer} from './../../components/Footer/Footer';
import HomeBanner from './../../components/HomeBanner/HomeBanner';
import {StaticNewsletter} from './../../components/Newsletter/StaticNewsletter';
import {Link} from 'gatsby';

const ContainerWrapper = styled.main`
  width: 100%;
  max-width: 1200px;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  margin-top: 35px;
  position: relative;
  min-height: calc(100vh - 515px);

  @media (min-width: 769px) {
    min-height: calc(100vh - 415px);
  }
`

const NewsletterModal = styled.div`
  position: absolute;
  width: 720px;
  height: 340px;
  background: #C4C4C4;
  border: 1px solid #000000;
  box-sizing: border-box;
  box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  top: calc(50vh - 170px);
  left: calc(50vw - 360px);
  z-index: 10;
  display: ${props => props.visibility ? 'block' : 'none'};
`;

const CookiesModal = styled.div`
    height: 135px;
    position: fixed;
    left: 15px;
    bottom: 15px;
    width: 335px;
    background: #c4c4c4;
    border: 1px solid #00000001;
    box-sizing: border-box;
    box-shadow: 0px 5px 5px rgba(0,0,0,0.8);
    border-radius: 5px;
    padding: 15px;
    font-family: Roboto;
  font-style: normal; 
  font-weight: bold;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  display: ${props => props.visibility ? 'block' : 'none'}
`;

const NewsletterButton = styled.button`
height: 35px;
    text-align: center;
    border: none;
    border-radius: 10px;
    color: #fff;
    background: #2C3E50;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 15px;
    text-transform: uppercase;
`;

class MainTemplates extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarVisibility: false,
      blurVisibility: false,
      index: 0,
      modalVisibility: false,
      cookies: false
    }
  }

  componentDidMount() {
    if (typeof window !== `undefined`) {
      let cookies = JSON.parse(window.localStorage.getItem('cookies-policy'));  
      
      this.setState({
        ...this.state,
        cookies: cookies ? false : true
      })
    }
  }

  changeSidebarVisibility() {
    this.setState({
      ...this.state, 
      sidebarVisibility: !this.state.sidebarVisibility, 
      blurVisibility: !this.state.blurVisibility
    });
  }
  
  changeModalVisibility() {
    this.setState({
      ...this.state, 
      modalVisibility: !this.state.modalVisibility, 
      blurVisibility: !this.state.blurVisibility
    });
  }

  cookiesModalClick() {
    window.localStorage.setItem('cookies-policy', 'true');
    this.setState({
      ...this.state,
      cookies: false
    })
  }

  render() {
    return (
      <>
        <Header clickBurgerIcon={() => this.changeSidebarVisibility()}/>
        {this.props.banner ? <HomeBanner /> : <></>}
        <ContainerWrapper>
          {this.props.children}
        </ContainerWrapper>
        <Footer />
        <Sidebar visibility={this.state.sidebarVisibility} clickBurgerIcon={() => this.changeSidebarVisibility()} />
        <Blur visibility={this.state.blurVisibility} />
        <Blur visibility={this.props.showNewsletterModal} blurClick={this.props.closeNewsletterModal} />
        <NewsletterModal visibility={this.props.showNewsletterModal}>
          <StaticNewsletter />
        </NewsletterModal>
        <CookiesModal visibility={this.state.cookies}>
          <p>Korzystam z plików cookies na stronie w celu zapewnienia jej prawidłowego działania oraz po to, by analizować ruch i prowadzić działania marketingowe. Szczegóły znajdziesz w <Link to={'/polityka-prywatnosci'}>polityce prywatności</Link>.</p>
          <NewsletterButton onClick={() => this.cookiesModalClick()}>Zgadzam się</NewsletterButton>
        </CookiesModal>
      </>
    )
  }
}

MainTemplates.propTypes = {
  children: PropTypes.node.isRequired,
}
  
export default MainTemplates;
