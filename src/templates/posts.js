import React from "react"
import { graphql, StaticQuery } from "gatsby"
import MainTemplates from "./MainTemplates/MainTemplates"
import SEO from "../components/seo";
import { HighlightedPost } from "../components/Post/HighlightedPost/HighlightedPost";
import styled from 'styled-components';

const PostsWrapper = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    
    @media (min-width: 769px) {
      grid-template-columns: repeat(2, 1fr);
      grid-template-rows: 1fr;
      grid-column-gap: 80px;
      grid-row-gap: 0px;
    }
`;

const PostPage = () => {
  return (
    <StaticQuery 
        query={graphql`
            query{
                allGhostPost {
                    edges {
                        node {
                            id
                            slug
                            visibility
                            created_at
                            feature_image
                            title
                            excerpt
                        }
                    }
                }
            }
        `}

        render={(data) => {
            const postList = data.allGhostPost.edges
                .map((node, index) => <HighlightedPost 
                    title={node.node.title} 
                    excerpt={node.node.excerpt} 
                    createdAt={node.node.created_at} 
                    image={node.node.feature_image}
                    key={index}
                    slug={node.node.slug}></HighlightedPost>
                );

            return(
                <>
                    <SEO title={'Posty'}/>
                    <MainTemplates>
                        <PostsWrapper>
                            {postList}
                        </PostsWrapper>
                    </MainTemplates>
                </>
            );
        }}
    />
  );
}

export default PostPage;
