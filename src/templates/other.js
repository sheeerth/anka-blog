import React from "react"
import MainTemplates from "./MainTemplates/MainTemplates"
import SEO from "../components/seo";
import styled from 'styled-components';

const HtmlWrapper = styled.section`
  max-width: 768px;
  margin: auto;
`;

const OtherPage = ({pageContext}) => {
  return (<>
    <SEO title={pageContext.title}/>
    <MainTemplates>
      <HtmlWrapper dangerouslySetInnerHTML={{__html: pageContext.html}}></HtmlWrapper>
    </MainTemplates>
  </>);
}

export default OtherPage;