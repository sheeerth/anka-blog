import React, {useState} from "react"
import MainTemplates from "./MainTemplates/MainTemplates"
import SEO from "../components/seo";
import styled from 'styled-components';
import PostSidebar from "../components/PostSideBar/PostSidebar";
import {Link} from 'gatsby'; 

const TitleWrapper = styled.h2`
  font-size: 40px;
  font-weight: bold;
  font-family: 'Raleway', sans-serif;
  margin-bottom: 55px;
`;

const ContentWrapper = styled.div`
    display: block;
  
  @media (min-width: 769px) {
    display: grid;
    grid-template-columns: auto 270px;
    column-gap: 50px;
  }
`;

const ImageWrapper = styled.img`
    height: 100%;
    max-width: 768px;
    text-align: center;
    display: flex;
    justify-content: center;
    margin: auto;
`;

const HtmlWrapper = styled.div`
  max-width: 768px;
  font-size: 18px;
  margin-left: auto;
  margin-right: auto;
  margin-top: 35px;

  p {
    margin-top: 30px;
    margin-bottom: 30px;
  }
`;

const BreakLine = styled.div`
  background: #C4C4C4;
  width: 100%;
  height: 1px;
  margin-top: 15px;
  margin-bottom: 15px;
  display: none;
  
  @media (min-width: 769px) {
    display: block;
  }
`;

const PostNavigation = styled.div`
  /* justify-content: space-between; */
  display: none;
  margin-bottom: 35px;
  padding-right: 15px;
  padding-left: 15px;
  justify-content: ${props => {
    if (props.visibility[0] && props.visibility[1]) {
      return 'space-between';
    }

    if (props.visibility[0]) {
      return 'flex-end';
    }

    if(props.visibility[1]) {
      return 'flex-start';
    }
  }};
  
  @media (min-width: 769px) {
    display: flex;
  }
`;

const PostNavigationElement = styled(Link)`
  display: flex;
  width: 50%;
  justify-content: flex-start;
  text-decoration: none !important;
`;

const PostNavigationElementNext = styled(PostNavigationElement)`
  flex-direction: row-reverse;
`;

const PostNavigationImage = styled.img`
  max-height: 70px;
  width: 120px;
`;

const PostNavigationInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 15px;
  margin-left: 15px;
`;

const PostNavigationInfoReverse = styled(PostNavigationInfo)`
  align-items: flex-end;
`;

const PostNavigationNavigation = styled.span`
  font-family: Raleway;
  font-style: normal;
  font-weight: bold;
  font-size: 8px;
  line-height: 9px;
  color: #000000;
  text-transform: uppercase;
  margin-bottom: 5px;
  display: flex;
  align-items: center;

  svg {
    font-size: 10px;
    margin-right: 5px;
  }
`;

const PostNavigationNavigationRevers = styled(PostNavigationNavigation)`

  svg {
    margin-left: 5px;
    margin-right: 0;
  }
`;

const PostNavigationTitle = styled.div`
  font-family: Raleway;
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 18px;
  color: #000000;
`;

const PostPage = ({pageContext}) => {
  const [newsletterModal, setNewsletterModal] = useState(false);

  const changeModalVisibility = () => {
    setNewsletterModal(!newsletterModal);
  }

  const previousPost = pageContext.previous !== null ? 
    (
    <PostNavigationElementNext to={`/post/${pageContext.previous.slug}`}>
      <PostNavigationImage src={pageContext.previous.feature_image} />
      <PostNavigationInfoReverse>
        <PostNavigationNavigationRevers> następny <i class="fas fa-arrow-right"></i></PostNavigationNavigationRevers>
        <PostNavigationTitle>{pageContext.previous.title}</PostNavigationTitle>
      </PostNavigationInfoReverse>
    </PostNavigationElementNext>
    ) : '';

  const nextPost = pageContext.next !== null ? 
    (
    <PostNavigationElement to={`/post/${pageContext.next.slug}`}>
      <PostNavigationImage src={pageContext.next.feature_image} />
      <PostNavigationInfo>
        <PostNavigationNavigation>  <i class="fas fa-arrow-left"></i> poprzedni </PostNavigationNavigation>
        <PostNavigationTitle>{pageContext.next.title}</PostNavigationTitle>
      </PostNavigationInfo>
    </PostNavigationElement>
    ) : '';

  const [previousVisibility, nextVisibility] = [pageContext.previous !== null, pageContext.next !== null];

  return (<>
    <SEO title={pageContext.metaTitle}/>
    <MainTemplates showNewsletterModal={newsletterModal} closeNewsletterModal={changeModalVisibility}>
      <TitleWrapper>{pageContext.title}</TitleWrapper>
      <ContentWrapper>
        <div>
          <ImageWrapper alt={`${pageContext.slug}-image`} src={pageContext.image}/>
          <HtmlWrapper dangerouslySetInnerHTML={{ __html: pageContext.html }}/>
        </div>
        <PostSidebar tags={pageContext.tags} date={pageContext.createdAt} showModal={changeModalVisibility}/>
      </ContentWrapper>
      <BreakLine />
      <PostNavigation visibility={[previousVisibility, nextVisibility]}>
        {nextPost}
        {previousPost}
      </PostNavigation>
    </MainTemplates>
  </>);
}

export default PostPage;
