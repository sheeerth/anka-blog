import React from 'react'
import styled from 'styled-components';
import * as moment from 'moment';
import 'moment/locale/pl'
import facebook from './../../images/image 1.png';
import { Link } from 'gatsby';

const PostSidebarWrapper = styled.div`
    width: 270px;
    display: none;
    flex-direction: column;
    
    @media (min-width: 769px) {
      display: flex;
    }
`;

const DateWrapper = styled.span`
    margin-bottom: 30px;
    font-family: Raleway;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 28px;
    color: #000000;
    padding: 0;
`;

const TagsWrapper = styled.div`
    display: flex;
    flex-wrap: wrap; 
    margin-bottom: 35px;   
`;

const TagWrapper = styled.div`
  background-color: #2C3E50;
  color: #45B39D;
  margin-right: 15px;
  padding: 5px 10px;
  border-radius: 5px;
  text-transform: uppercase;
  font-weight: bold;
  font-family: 'Raleway', sans-serif;
  font-size: 15px;
  margin-top: 10px;
`;

const Image = styled.img`
    margin-bottom: 35px;
    margin-top: 35px;
`;

const ButtonEmail = styled.button`
    height: 50px;
    text-align: left;
    border: none;
    border-radius: 10px;
    color: #fff !important;
    background: #2C3E50;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 21px;
    text-transform: uppercase;
    margin-bottom: 35px;
    align-content: center;
    align-items: center;
    display: flex;
    padding-left: 15px;
    font-family: Roboto;
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 21px;

    color: #FFFFFF;

    svg {
        font-size: 32px;
        margin-right: 15px;
    }
`;

const Button = styled.a`
    height: 50px;
    text-align: left;
    border: none;
    border-radius: 10px;
    color: #fff !important;
    background: #2C3E50;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 21px;
    text-transform: uppercase;
    margin-bottom: 35px;
    align-content: center;
    align-items: center;
    display: flex;
    padding-left: 15px;
    font-family: Roboto;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    /* identical to box height */

    text-align: center;
    text-decoration-line: underline;

    svg {
        font-size: 32px;
        margin-right: 15px;
    }
`;

const ButtonInstagram = styled(Button)`
    text-transform: lowercase;
    text-decoration: underline !important;
`;

const PostSidebar = ({date, tags, showModal}) => {
    const taging = tags.map((tag, index) => <TagWrapper key={index}>{tag.name}</TagWrapper>);
    moment.locale('pl');
    const createdDate = moment(date).format('D MMMM YYYY');

    const openNewsletter = () => {
        console.log('test');
    }

    return (
        <PostSidebarWrapper>
            <DateWrapper>{createdDate}</DateWrapper>
            <TagsWrapper>
                {taging}
            </TagsWrapper>
            <div className="fb-page" data-href="https://www.facebook.com/blog.mam.depresje" data-tabs="" data-width=""
                 data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                 data-show-facepile="true">
              <blockquote cite="https://www.facebook.com/blog.mam.depresje" className="fb-xfbml-parse-ignore"><a
                href="https://www.facebook.com/blog.mam.depresje">Mam depresję</a></blockquote>
            </div>
            <ButtonInstagram href="https://www.instagram.com/blog_mam_depresje/?hl=pls">
                <i className="fab fa-instagram"></i>
                @blog_mam_depresje
            </ButtonInstagram>
            <ButtonEmail onClick={showModal}>
                <i className="far fa-envelope"></i>
                Dołacz
            </ButtonEmail>
        </PostSidebarWrapper>
    );
}

export default PostSidebar;
