import React from 'react';
import styled from 'styled-components';
import Hamburger from '../Header/Hamburger/Hamburger';
import { StaticQuery, graphql } from 'gatsby';
import MenuElement from './MenuElement/MenuElement';


const SidebarWrapper = styled.div`
    width: 85%;
    max-width: 300px;
    height: 100%;
    background: #2C3E50;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 10;
    display: ${props => props.visibility ? 'block' : 'none'};

    @media (min-width: 769px) {
        display:none;
    }
`;

const HamburgerWrapper = styled.div`
    height: 65px;
    width: 100%;
    display: flex;
    flex-direction: row-reverse;
`;

const ListElementWrapper = styled.ul`
    margin: 0;
    padding: 15px;
    margin-top: 30px;
    list-style: none;
    text-decoration: none;
    height: 100%;
    display: flex;
    flex-direction: column;
`;


const Sidebar = ({visibility, clickBurgerIcon}) => {
    return (
        <StaticQuery 
            query={graphql`
                query{
                    allGhostSettings {
                        edges {
                            node {
                                navigation {
                                    label
                                    url
                                }
                            }
                        }
                    }
                }
            `}

            render={data => {
                    const navigationElement = data.allGhostSettings.edges[0].node.navigation.map((nav, index) => <MenuElement url={nav.url} label={nav.label} key={index}/>);

                    return(
                        <SidebarWrapper visibility={visibility}>
                            <HamburgerWrapper>
                                <Hamburger clicked={clickBurgerIcon}/>
                            </HamburgerWrapper>
                            <ListElementWrapper>
                                {navigationElement}
                            </ListElementWrapper>
                        </SidebarWrapper>
                    );
                }
            }
        />
        
    );
}

export default Sidebar;
