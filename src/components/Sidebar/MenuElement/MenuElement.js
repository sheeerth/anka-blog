import React from 'react'
import styled from 'styled-components';
import { Link } from 'gatsby';

const production = process.env.NODE_ENV === 'production' ? true : false;

const LinkWrapper = styled(Link)`
    text-decoration: none;
    margin: 0 15px;
    color: #45B39D;
    font-family: Raleway;
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 35px;
    align-items: center;
    letter-spacing: 0.05em;
    text-transform: uppercase;
    
    @media (min-width: 769px) {
      line-height: 65px;
    }

    :focus {
        text-decoration: none;
        color: #1FF1C7;
    }

    :hover {
        text-decoration: none;
        color: #1FF1C7;
    }
`;

const MenuElement = ({url, label}) => {
    const relativeUrl = production ? '/' + url.split('/').slice(3).join('/') : '/' + url.split('/').slice(3).join('/');

    return (
        <LinkWrapper to={relativeUrl}>{label}</LinkWrapper>

    );    
}

export default MenuElement;
