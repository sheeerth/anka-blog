import React from 'react'
import styled from 'styled-components';

const BlurWrapper = styled.div`
    background: rgba(0, 0, 0, 0.4);
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 5;
    display: ${props => props.visibility ? 'block' : 'none'};
`;

const Blur = ({visibility, blurClick}) => {
    return (
        <BlurWrapper visibility={visibility} onClick={blurClick}></BlurWrapper>
    );
}

export default Blur;
