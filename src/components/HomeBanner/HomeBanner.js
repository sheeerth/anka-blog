import React from 'react';
import styled from 'styled-components';
import BannerBlog from './../../images/blog-banner.png';

const Banner = styled.div`
    display: none;
    width: 100%;
    height: 387px;
    background: #2c3e50;
    justify-content: center;
    
    @media (min-width: 769px) {
      display: flex;  
    }
`;

const ImgBanner = styled.img`
    height: auto;
    width: 100%;
    margin: auto;
    
    @media (min-width: 1200px) {
      max-height: calc(100% - 70px);
      width: auto;
    }
`;

const HomeBanner = (params) => {
    return (
        <Banner>
            <ImgBanner src={BannerBlog} />
        </Banner>
    )
}

export default HomeBanner;
