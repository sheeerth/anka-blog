import React from 'react'
import styled from 'styled-components';
import {HighlightedPost} from '../../components/Post/HighlightedPost/HighlightedPost'
import { StaticQuery, graphql } from 'gatsby';
import {StaticNewsletter} from '../Newsletter/StaticNewsletter';

const OtherPostWrapper = styled.section`

  @media (min-width: 769px) {
    display: grid;
    grid-template-columns: repeat( auto-fit, minmax(350px, 47.5%) );
    width: 100%;
    grid-column-gap: 5%;
    grid-row-gap: 60px;
  }
`;

const NewestPostHeadline = styled.h2`
  font-family: 'Raleway', sans-serif;
  margin-top: 50px;
  margin-bottom: 25px;
  text-transform: uppercase;
  font-weight: bold;
  font-style: normal;
  font-size: 16px;
  line-height: 19px;
  letter-spacing: 0.03em;
  color: #000000;
`;

const MainPost = styled.div`
  display: grid;
  grid-template-rows: repeat(2, auto);
  grid-row-gap: 35px;
  
  @media (min-width: 769px) {
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 1fr;
    grid-column-gap: 80px;
    grid-row-gap: 0;
  }
`;

function OtherPost({posts}) {
    const carouselList = posts.slice(0,6).map((node, index) => <HighlightedPost 
        title={node.node.title} 
        excerpt={node.node.excerpt} 
        createdAt={node.node.created_at} 
        image={node.node.feature_image}
        key={index}
        slug={node.node.slug}></HighlightedPost>
    );
  
    return (
      <OtherPostWrapper>
        {carouselList}
      </OtherPostWrapper>
    );
  }

const Home = ({postsCarusel}) => {
    return (
        <StaticQuery
          query={graphql`
            query{
              allGhostPost(filter: {featured: {eq: true}, visibility: {eq: "public"}}) {
                edges {
                  node {
                    id
                    slug
                    html
                    visibility
                    created_at
                    feature_image
                    title
                    featured
                    excerpt
                  }
                }
              }
            }
          `} 

          render={data => {            
            return(
              <>
                <MainPost>
                  <HighlightedPost
                      title={data.allGhostPost.edges[0].node.title} 
                      excerpt={data.allGhostPost.edges[0].node.excerpt} 
                      createdAt={data.allGhostPost.edges[0].node.created_at} 
                      image={data.allGhostPost.edges[0].node.feature_image}
                      slug={data.allGhostPost.edges[0].node.slug}>
                  </HighlightedPost>
                  <StaticNewsletter/>
                </MainPost>
                <NewestPostHeadline>Najnowsze wpisy</NewestPostHeadline>
                <OtherPost posts={postsCarusel.allGhostPost.edges} />
              </>
            );
          }}
        />
    )
}

export default Home;
