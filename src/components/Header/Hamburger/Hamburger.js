import React from "react"
import styled from "styled-components";

const HamburgerWrapper = styled.a`
    display: flex;
    flex: 50% 50%;
    justify-content: center;
    width: 65px;
    height: 100%;
    align-items: center;
    flex-wrap: wrap;
    padding: 13px;
    box-sizing: border-box;

    @media (min-width: 769px) {
        display: none;
    }
`;

const DotsWrapper = styled.div`
    flex-basis: 50%;
`;

const Dots = styled.div`
    background: #45B39D;
    border-radius: 50%;
    width: 10px;
    height: 10px;
`;

const Hamburger = ({clicked}) => {
    return(
        <HamburgerWrapper onClick={clicked}>
            <DotsWrapper>
                <Dots />
            </DotsWrapper>
            <DotsWrapper>
                <Dots />
            </DotsWrapper>
            <DotsWrapper>
                <Dots />
            </DotsWrapper>
            <DotsWrapper>
                <Dots />
            </DotsWrapper>
        </HamburgerWrapper>
    )
}

export default Hamburger;
