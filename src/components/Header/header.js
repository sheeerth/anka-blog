import { StaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from 'styled-components';
import Hamburger from "./Hamburger/Hamburger";
import MenuElement from './../Sidebar/MenuElement/MenuElement';
import Logo from './../../images/mam-depresje.png';

const StyledHeader = styled.header`
  height: 65px;
  background: #2C3E50;
  color: #45B39D;
  font-family: Raleway;
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 65px;
  align-items: center;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  display: flex;
`

const HeaderInternal = styled.div`
  display: none;
  justify-content: flex-start;
  width: 1440px;
  margin: auto;
  padding-left: 120px;
  
  @media (min-width: 769px) {
    display: flex;    
  }
`;

const LogoImg = styled.img`
  max-height: 65px;
  width: auto;
  margin-top: -1px;
`;

const ListElementWrapper = styled.ul`
    list-style: none;
    text-decoration: none;
    margin: 0;
    height: 100%;
    display: none;

    @media (min-width: 769px) {
      display: flex;
      padding: 0;
    }
`;

const Header = ({clickBurgerIcon}) => {
  return(
    <StaticQuery 
        query={graphql`
                query{
                    allGhostSettings {
                        edges {
                            node {
                                navigation {
                                    label
                                    url
                                }
                            }
                        }
                    }
                }
            `}

        render={(data) => {
            const navigationElement = data.allGhostSettings.edges[0].node.navigation.map((nav, index) => <MenuElement url={nav.url} label={nav.label} key={index}/>);

            return(
              <StyledHeader>
                <Hamburger clicked={clickBurgerIcon} />
                <HeaderInternal>
                  <LogoImg src={Logo} />
                  <ListElementWrapper>
                    {navigationElement}
                  </ListElementWrapper>  
                </HeaderInternal>
              </StyledHeader>
            );
          }
        }
    /> 
  );
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
