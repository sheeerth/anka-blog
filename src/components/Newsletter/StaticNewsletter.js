import React from 'react'
import styled from 'styled-components';
import NewsletterForm from './NewsletterForm';

const Newsletter = styled.div`
    background: #E2E2E2;
    height: 100%;
    width: 100%;
    display: flex;
    align-content: center;
`;
 
export const StaticNewsletter = () =>  {
    return (<Newsletter>
        <NewsletterForm/>
    </Newsletter>);
}