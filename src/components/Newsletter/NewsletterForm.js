import React, {useState} from 'react'
import styled from 'styled-components';

const NewsletterFormWrapper = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 360px;
    text-align: center;
    margin: auto;
    padding: 15px;
`;

const NewsletterTitle =  styled.h5`
    font-family: Raleway;
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 21px;
    letter-spacing: 0.05em;
    color: #2C3E50;
    text-transform: uppercase;
    margin-bottom: 35px;
`;

const NewsletterInput = styled.input`
    margin-bottom: 15px;
    height: 50px;
    border: none;
    border-radius: 10px;
    text-align: center;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 21px;
    text-align: center;
    
    &.error {
      border: 1px solid #a40202;
    }
`;

const NewsletterInfo = styled.span`
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    text-align: center;
    color: #2C3E50;
    margin-bottom: 15px;
`;

const NewsletterButton = styled.button`
    height: 50px;
    text-align: center;
    border: none;
    border-radius: 10px;
    color: #fff;
    background: #2C3E50;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 21px;
    text-transform: uppercase;
    margin-right: 15px;
    margin-left: 15px;
`;

const ValidationMessage = styled.span`
  color: #a40202;
  font-family: Roboto;
  font-size: 12px;
  margin-top: 8px;
  margin-bottom: 8px;
  display: block;
`;

const ConfirmMessage = styled.span`
  font-family: Raleway;
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 21px;
    letter-spacing: 0.05em;
    color: #2C3E50;
    text-transform: uppercase;
    margin-bottom: 35px;
`;

const NewsletterForm = () => {
    const [Name, setName] = useState('');
    const [Email, setEmail] = useState('');
    const [Confirm, setConfirm] = useState(false);
    const [ValidInfo, setValidInfo] = useState('');
    const [NameClass, setNameClass] = useState('');
    const [EmailClass, setEmailClass] = useState('');

    const validationEmail = email => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const validationForm = subscriber => {
        return subscriber.firstName === undefined ||
            subscriber.firstName === '' ||
          subscriber.email === undefined;
    }

    const addSubscriber = () => {
        const subscriber = {
            firstName: Name,
            email: Email
        };

        setValidInfo('');
        setEmailClass('');
        setNameClass('');

            if (validationForm(subscriber)) {
                setValidInfo('Wszystkie pola sa wymagane');
                setNameClass('error');
                setEmailClass('error');
                return;
            }

            if (!validationEmail(subscriber.email)) {
                setValidInfo('Niepoprawny Email');
                setEmailClass('error');
                return;
            }

            fetch('https://hook.integromat.com/48xmj6xa8rinlp7c88xcxoxh9nvdmeeq', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(subscriber),
            }).then(() => {
                setName('');
                setEmail('');
                setConfirm(true);
            });
    }

    const form = Confirm ? (
      <ConfirmMessage>Dziekuje za zapisanie sie do Newslettera!</ConfirmMessage>
    ) : (
      <>
        <NewsletterTitle>Newsletter</NewsletterTitle>
        <NewsletterInput className={NameClass} placeholder="Imię" value={Name} onChange={(e) => setName(e.target.value)} />
        <NewsletterInput className={EmailClass} placeholder="Email" value={Email} onChange={(e) => setEmail(e.target.value)} />
        <NewsletterInfo>
          Uzupełnienie powyższego pola stanowi zgodę na otrzymywanie
          od mamdepresje.com newslettera z treściami marketingowymi.
          Zgodę można wycofać w każdym czasie.
        </NewsletterInfo>
          <ValidationMessage>{ValidInfo}</ValidationMessage>
        <NewsletterButton onClick={addSubscriber}>Dołącz</NewsletterButton>
      </>
    )

    return (
        <NewsletterFormWrapper>
          {form}
        </NewsletterFormWrapper>
    );
}

export default NewsletterForm;
