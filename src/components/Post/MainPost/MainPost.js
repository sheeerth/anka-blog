import React from 'react';
import Col from '@bootstrap-styled/v4/lib/Col';
import Img from '@bootstrap-styled/v4/lib/Img';
import styled from 'styled-components';
import {rowStyle, colStyle} from './../../../templates/globalStyles';
import {Post} from './../Post';
import {Link} from 'gatsby';
import moment from 'moment';

const SectionWrapper = styled.section`
    margin-top: 15px;
    margin-bottom: 15px;
`;

const RowWrapper = styled.div`
    ${rowStyle}

    @media (min-width: 769px) {
        grid-row: 1 / 4;
    }
`;

const HeaderPostWrapper = styled.header`
    ${rowStyle}
`;

const ArticleWrapper = styled.article`
    ${rowStyle}
`;

const H2Wrapper = styled.h2`
    ${colStyle}
    margin-top: 15px;
    margin-bottom: 10px;
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
`;

const ParagraphWrapper = styled.p`
    ${colStyle}
    margin: 0;
`;

const FooterWrapper = styled.footer`
    ${rowStyle}
`;

const DateWrapper = styled.span`
    ${colStyle}
    font-size: 10px;
    margin-top: 10px;
`;

const LinkWrapper = styled(Link)`
    text-decoration: none;
    cursor: pointer;
    color: #000;

    :hover {
        text-decoration: none;
        color: #000;
    }

    @media (min-width: 769px) {
        display: grid;
        grid-template-columns: repeat(2, calc(50% - 8px));
        grid-column-gap: 16px;
        grid-template-rows: minmax(4em,6em) 225px 25px;
    }
`;

export class MainPost extends Post{
    render() {
        const createdDate = moment(this.props.createdAt).format('DD MM YYYY'); 

        return (
            <SectionWrapper>
                <LinkWrapper to={`/post/${this.props.slug}`}>
                    <RowWrapper>
                        <Col>
                            <Img thumbnail alt={`${this.props.slug}-image`}  src={this.props.image}></Img>
                        </Col>
                    </RowWrapper>
                    <HeaderPostWrapper>
                        <H2Wrapper>{this.props.title}</H2Wrapper>
                    </HeaderPostWrapper>
                    <ArticleWrapper>
                        <ParagraphWrapper>
                            {this.props.excerpt}
                        </ParagraphWrapper>
                    </ArticleWrapper>
                    <FooterWrapper>
                        <DateWrapper>
                            {createdDate}
                        </DateWrapper>
                    </FooterWrapper>
                </LinkWrapper>
            </SectionWrapper>
        );
    }

}
