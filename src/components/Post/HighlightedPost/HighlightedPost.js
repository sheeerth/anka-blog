import React from 'react';
import Col from '@bootstrap-styled/v4/lib/Col';
import Img from '@bootstrap-styled/v4/lib/Img';
import styled from 'styled-components';
import {rowStyle, colStyle} from '../../../templates/globalStyles';
import {Post} from '../Post';
import {Link} from 'gatsby';

const SectionWrapper = styled.section`
    margin-bottom: 35px;
`;

const RowWrapper = styled.div`
    ${rowStyle}
    grid-row: 1/4;
`;

const HeaderPostWrapper = styled.header`
    ${rowStyle}
    padding-right: 15px;
    padding-left: 15px;
`;

const ArticleWrapper = styled.article`
    ${rowStyle}
    padding-right: 15px;
    padding-left: 15px;
`;

const H3Wrapper = styled.h3`
    ${colStyle}
    width: 70%;
    font-size: 18px;
    margin-top: 15px;
    margin-bottom: 10px;
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
`;

const ParagraphWrapper = styled.p`
    ${colStyle}
    margin: 0;
    font-size: 15px;
`;

const LinkWrapper = styled(Link)`
    text-decoration: none;
    cursor: pointer;
    color: #000;

    :hover {
        text-decoration: none;
        color: #000;
    }
`;

export class HighlightedPost extends Post{
    render() {
        return (
            <SectionWrapper>
                <LinkWrapper to={`/post/${this.props.slug}`}>
                    <RowWrapper>
                        <Col>
                            <Img thumbnail alt={`${this.props.slug}-image`}  src={this.props.image}></Img>
                        </Col>
                    </RowWrapper>
                    <HeaderPostWrapper>
                        <H3Wrapper>{this.props.title}</H3Wrapper>
                    </HeaderPostWrapper>
                    <ArticleWrapper>
                        <ParagraphWrapper>
                            {this.props.excerpt}
                        </ParagraphWrapper>
                    </ArticleWrapper>
                </LinkWrapper>
            </SectionWrapper>
        );
    }

}