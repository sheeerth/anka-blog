import React from 'react';
import styled from 'styled-components';
import { Link, StaticQuery} from 'gatsby';
import Cooperation from './../../images/logo-stowarzyszenie.png';

const FooterWrapper = styled.footer`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  background-color: #a0a0a0;
  bottom: 0;
  position: relative;
  padding-bottom: 25px;
  height: 150px;

  @media (min-width: 769px) {
    height: 90px;
  }
`;

const FooterContentWrapper = styled.div`
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;

  @media (min-width: 769px) {
    flex-direction: row;
  }
`;

const FooterCopyright = styled.div`
  height: 25px;
  font-size: 10px;
  text-align: center;
  background-color: #505050;
  margin-right: -15px;
  margin-left: -15px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: rgb(229, 229, 229);
  position: absolute;
  bottom: 0;
  width: 100%;
`;

const FooterCopyrightLink = styled(Link)`
    font-family: 'Raleway', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    /* identical to box height */
    color: #FFFFFF;
    margin-left: 5px;
    margin-right: 5px;
    cursor: pointer;
    text-decoration: none;

    :hover {
      color: white;
      text-decoration: none;
    }
`;

const FooterHeart = styled.span`
    color: red;
    margin-right: 5px;
    margin-left: 5px;
`;

const IconWrapper = styled.div`
  color: #FFF;
  margin: 0 10px;
  font-size: 24px;
  line-height: 24px;

  :first-of-type {
    margin-left: 0;
  }

  a {
    color: #FFF;
    text-decoration: none;

    :focus {
      color: #FFF8;
    }

    :hover {
      color: #FFFC;
    }
  }

  @media (min-width: 769px) {
    margin-top: 0;
  }
`;

const IconsWrapper = styled.section`
  display: flex;
  align-items: flex-start;
`;

const FooterNavigationWrapper = styled.section`
  display: none;
  
  @media (min-width: 769px) {
      display: flex;
  }
`;

const FooterLinkWrapper = styled(Link)`
  color: #2C3E50;
  text-decoration: none;
  font-family: 'Raleway', sans-serif;

    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 18px;
    margin-right: 40px;
    margin-left: 40px;

  :focus {
    color: white;
    text-decoration: none;
  }

  :hover {
    color: white;
    text-decoration: none;
  }
`;

const CooperationWrapper = styled.div`
  img.logo-cooperation {
    width: auto;
    height: 35px;
  }
`;

const production = process.env.NODE_ENV === 'production' ? true : false;

const FotterNavigation = () => {
  return(
    <StaticQuery 
        query={graphql`
                query{
                    allGhostSettings {
                        edges {
                            node {
                                navigation {
                                    label
                                    url
                                }
                            }
                        }
                    }
                }
            `}

        render={(data) => {
            const navigationElement = data.allGhostSettings.edges[0].node.navigation.map((nav, index) => {
              const relativeUrl = production ? '/' + nav.url.split('/').slice(3).join('/') : '/' + nav.url.split('/').slice(3).join('/');
              if (nav.label === 'Strona Główna') {
                  return;
              }
              return <FooterLinkWrapper to={relativeUrl} label={nav.label} key={index}>{nav.label}</FooterLinkWrapper>
            });

            return(
              <FooterNavigationWrapper>
                {navigationElement}
              </FooterNavigationWrapper>
            );
          }
        }
    /> 
  );
}

export const Footer = () => {  
    const privacyPoliticsLink = '/polityka-prywatnosci';
  
    return (
      <FooterWrapper>
        <FooterContentWrapper>
          <IconsWrapper>
            <IconWrapper>
              <a href="https://www.facebook.com/blog.mam.depresje">
                <i className="fab fa-facebook-square"></i>
              </a>
            </IconWrapper>
            <IconWrapper>
              <a href="https://www.instagram.com/blog_mam_depresje/">
                <i className="fab fa-instagram"></i>
              </a>
            </IconWrapper>
          </IconsWrapper>
          <FotterNavigation />
          <CooperationWrapper>
            <a href={'http://widzewiecej.pl/'}>
              <img className="logo-cooperation" src={Cooperation} />
            </a>
          </CooperationWrapper>
        </FooterContentWrapper>
        <FooterCopyright>
            <FooterContentWrapper>
                <FooterCopyrightLink to={privacyPoliticsLink}>Polityka Prywatności</FooterCopyrightLink>
                {/* <FooterCopyrightLink>Regulamin</FooterCopyrightLink> */}
                <FooterCopyrightLink>Made with <FooterHeart>❤</FooterHeart> by Bartosz Gawroński</FooterCopyrightLink>
            </FooterContentWrapper>
        </FooterCopyright>
      </FooterWrapper>
    )
}


