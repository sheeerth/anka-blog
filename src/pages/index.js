import React from "react"

import MainTemplates from "../templates/MainTemplates/MainTemplates"
import Home from "../components/Home/Home"
import SEO from "../components/seo";
import { graphql } from 'gatsby';

const IndexPage = ({data}) => {
  return(
    <>
      <SEO title="Strona Główna" />
      <MainTemplates banner={true}>
        <Home postsCarusel={data}></Home>
      </MainTemplates>
    </>
  );
}

export default IndexPage


export const query = graphql`
  query{
      allGhostPost(sort: {fields: published_at, order: DESC}, filter: {featured: {eq: false}, visibility: {eq: "public"}}) {
          edges {
              node {
                  id
                  slug
                  html
                  visibility
                  created_at
                  feature_image
                  title
                  featured
                  excerpt
                  published_at
              }
          }
      }
  }
`;
