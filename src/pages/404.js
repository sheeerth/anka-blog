import React from 'react'
import MainTemplates from '../templates/MainTemplates/MainTemplates'
import styled from 'styled-components';

const PageNotFoundWrapper = styled.div`
    max-width: 768px;
    margin: auto;
    text-align: center;
`;

const PageNotFoundHeader = styled.h2`
    font-size: 48px;
    font-family: 'Raleway', sans-serif;
    font-weight: bold;
    text-transform: uppercase;
    margin-bottom: 35px;
    margin-top: 100px;
`;

const PageNotFoundSpan = styled.span`
    font-size: 25px;
    text-transform: uppercase;
`;

const PageNotFoundPause = styled.div`
    margin-top: 50px;
    margin-bottom: 50px;
    display: block;
`;

const PageNotFound = (params) => {
    setTimeout(() => window.location.href = '/', 5000);

    return(
        <>
            <MainTemplates>
                <PageNotFoundWrapper>
                    <PageNotFoundHeader>Strona nie istnieje</PageNotFoundHeader>    
                    <PageNotFoundSpan>Za chwilę zostaniesz przekierowany na strone główną</PageNotFoundSpan>
                </PageNotFoundWrapper>
            </MainTemplates>
        </>
    )
}

export default PageNotFound;
