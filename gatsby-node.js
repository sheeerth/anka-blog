const path = require('path');

const production = process.env.NODE_ENV === 'production' ? true : false; 
// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = async ({ graphql, actions, reporter }) => {
    const { createPage } = actions

    console.log(process.env.NODE_ENV);
  
    // Query for markdown nodes to use in creating pages.
    const result = await graphql(
      `
        {
          allGhostPost {
            edges {
              node {
                id
                slug
                html
                visibility
                created_at
                feature_image
                title
                featured
                excerpt
                meta_title
                tags {
                  slug
                  url
                  postCount
                  name
                  id
                }
              }
              previous {
                feature_image
                slug
                title
              }
              next {
                slug
                feature_image
                title
              }
            }
          }
          allGhostSettings {
            edges {
              node {
                internal {
                  content
                  description
                  ignoreType
                  mediaType
                }
                navigation {
                  label
                  url
                }
                codeinjection_head
                codeinjection_styles
                codeinjection_foot
              }
            }
          }
          allGhostPage {
            edges {
              node {
                html
                slug
                url
                title
              }
            }
          }
        }
      `
    )
  
    // Handle errors
    if (result.errors) {
      reporter.panicOnBuild(`Error while running GraphQL query.`)
      return
    }  
    // Create pages for each markdown file.
    const blogPostTemplate = path.resolve(`src/templates/post.js`);
    result.data.allGhostPost.edges.forEach(({ node, previous, next}) => {
      const path = `/post/${node.slug}`
      createPage({
        path,
        component: blogPostTemplate,
        // In your blog post template's graphql query, you can use pagePath
        // as a GraphQL variable to query for data from the markdown file.
        context: {
          slug: node.slug,
          html: node.html,
          title: node.title,
          image: node.feature_image,
          tags: node.tags,
          createdAt: node.created_at,
          metaTitle: node.meta_title,
          next: next,
          previous: previous
        },
      })
    })

    const otherPageTemplate = path.resolve(`src/templates/other.js`);
    const postsPageTemplate = path.resolve(`src/templates/posts.js`);

    result.data.allGhostSettings.edges[0].node.navigation.forEach((el) => {
      const slug = el.url.split('/')[3];
      
      if (slug === undefined || slug === '') {
        return;
      }

      const path = production ? `/${el.url.split('/').slice(3).join('/')}` : `/${el.url.split('/').slice(3).join('/')}`;

      createPage({
        path,
        component: postsPageTemplate,
      })
    });

    result.data.allGhostPage.edges.forEach(({node}) => {
      const path = production ? `/${node.url.split('/').slice(3).join('/')}` : `/${node.url.split('/').slice(3).join('/')}`;

      createPage({
        path,
        component: otherPageTemplate,
        context: {
          html: node.html,
          slug:  node.slug,
          title: node.title
        }
      })
    })

  }
