// const mailchimp = require('@mailchimp/mailchimp_marketing');

// exports.onClientEntry = (_, pluginOptions) => {
//     console.log(pluginOptions);



//     async function run() {
//         const response = await mailchimp.ping.get();
//         console.log(response);
//     }
      
//     run();
// //   const { createNode } = actions
// //   // Data can come from anywhere, but for now create it manually
// //   const myData = {
// //     key: 123,
// //     foo: `The foo field of my node`,
// //     bar: `Baz`,
// //   }
// //   const nodeContent = JSON.stringify(myData)
// //   const nodeMeta = {
// //     id: createNodeId(`my-data-${myData.key}`),
// //     parent: null,
// //     children: [],
// //     internal: {
// //       type: `MyNodeType`,
// //       mediaType: `text/html`,
// //       content: nodeContent,
// //       contentDigest: createContentDigest(myData),
// //     },
// //   }
// //   const node = Object.assign({}, myData, nodeMeta)
// //   createNode(node)
// }

exports.onClientEntry = function(_, pluginParams) {
    const mailchimp = require('@mailchimp/mailchimp_marketing');
    mailchimp.setConfig({
        apiKey: pluginParams.apiKey,
        server: pluginParams.server
    })
    window.mailchimp = mailchimp;
    window.mailchimp.listID = pluginParams.listId
};