const environment = {
  production: process.env.NODE_ENV === 'production' ? true : false
};

module.exports = {
  siteMetadata: {
    title: `Mam Depresje`,
    description: `Blog opisujący codzienne zmagania z depresją`,
    author: `Bartosz Gawroński`,
    siteUrl: `https://mamdepresje.com`
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-165406032-1",
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: true,
        // Delays sending pageview hits on route update (in milliseconds)
        pageTransitionDelay: 0,
        cookieDomain: "mamdepresje.com",
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Blog Mam Depresje`,
        short_name: `Mam Depresje`,
        start_url: `/`,
        background_color: `#2C3E50`,
        theme_color: `#45B39D`,
        display: `minimal-ui`,
        icon: `src/images/mam-depresje.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-ghost`,
      options: {
        apiUrl:  environment.production ? `https://cms.mamdepresje.com` : `http://localhost:3002`,
        contentApiKey:  environment.production ? `4dba1c9c4c3da2b36ccdcef7b5` : `03963b1528561bf6b31b855175`,
        // apiUrl:  `https://cms.mamdepresje.com`,
        // contentApiKey: `4dba1c9c4c3da2b36ccdcef7b5`,
        version: `v3`
      },
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      },
    },
    {
      resolve: "gatsby-plugin-sentry",
      options: {
        dsn: "https://e23395dc36674a54b453802d400cf050@o382864.ingest.sentry.io/5212397",
        // Optional settings, see https://docs.sentry.io/clients/node/config/#optional-settings
        environment: process.env.NODE_ENV,
        enabled: (() => ["production", "stage"].indexOf(process.env.NODE_ENV) !== -1)()
      }
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: `https://mamdepresje.com`,
        sitemap: `https://mamdepresje.com/sitemap.xml`,
        policy: [{ userAgent: '*' }]
      }
    },
    {
      resolve: 'mailchimp-plugin',
      options: {
        apiKey: 'ddfe34ed3778fddc94af14409d782a3b-us17',
        server: 'us17',
        listId: 'c8342ff292'
      }
    }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
